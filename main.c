/**
 * Copyright (c) 2015 - 2018, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 * @defgroup pwm_example_main main.c
 * @{
 * @ingroup pwm_example
 *
 * @brief  PWM Example Application main file.
 *
 * This file contains the source code for a sample application using PWM.
 *
 *
 */

#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "app_error.h"
#include "bsp.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "app_pwm.h"
#include "nrf_drv_timer.h"
#include "nrf_drv_clock.h"

const nrf_drv_timer_t TIMER_LED = NRF_DRV_TIMER_INSTANCE(0);
const nrf_drv_timer_t TIMER_KEY = NRF_DRV_TIMER_INSTANCE(1);

typedef struct {
  uint32_t code;
  int32_t cnt;
} ir_t;
static volatile ir_t ir = {0, 0};

/**
 * @brief Handler for timer events.
 */
void timer_led_event_handler(nrf_timer_event_t event_type, void* p_context)
{
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
          nrf_gpio_pin_clear(11);
          NRF_PWM1->TASKS_STOP = 1;
          break;
        case NRF_TIMER_EVENT_COMPARE1:
          ir.cnt--;
          if(ir.cnt <= 0) {
            //NRF_TIMER0->TASKS_STOP = 1;
            nrf_drv_timer_pause(&TIMER_LED);
            nrf_gpio_pin_clear(12);
            ir.cnt = 0;
          } else if(ir.cnt == 1) {
            nrf_gpio_pin_set(11);
            NRF_PWM1->TASKS_SEQSTART[0] = 1;
            NRF_TIMER0->CC[0] = 1125; // 562.5usec
            NRF_TIMER0->CC[1] = 2*40500; // 40.5msec
          } else {

            nrf_gpio_pin_set(11);
            NRF_PWM1->TASKS_SEQSTART[0] = 1;
            NRF_TIMER0->CC[0] = 1125; // 562.5usec
            if(ir.code & 0x80000000UL) {
              NRF_TIMER0->CC[1] = 2*2250; // 225usec
            } else {
              NRF_TIMER0->CC[1] = 2*1125; // 1125usec
            }
            ir.code <<= 1;
          }

          break;

        default:
            //Do nothing.
            break;
    }
}
void ircode(uint32_t code)
{
  if(ir.cnt != 0) {
    return; 
  }

  ir.code = code;
  ir.cnt = 34;
  
  // startbit
  NRF_TIMER0->CC[0] = 2*9000; // 9msec
  NRF_TIMER0->CC[1] = 2*13500; // 13.5msec
  nrf_gpio_pin_set(11);
    nrf_drv_timer_clear(&TIMER_LED);
  NRF_PWM1->TASKS_SEQSTART[0] = 1;
    nrf_drv_timer_enable(&TIMER_LED);
}


void timer_key_event_handler(nrf_timer_event_t event_type, void* p_context)
{
    switch (event_type)
    {
        case NRF_TIMER_EVENT_COMPARE0:
        if(nrf_gpio_pin_read(BUTTON_3) == 0) { 
          ircode(0x4bb640bf);
        } else if(nrf_gpio_pin_read(BUTTON_4) == 0) { 
          ircode(0x4bb6c03f);
        }
        break;
        default:
        ;
    }
}


int main(void)
{
    ret_code_t err_code;
    nrf_gpio_cfg_output(BSP_LED_0);
    nrf_gpio_cfg_output(BSP_LED_0);
//     nrf_gpio_cfg_output(BSP_LED_1);
    nrf_gpio_cfg_input(BUTTON_1, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(BUTTON_2, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(BUTTON_3, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_input(BUTTON_4, NRF_GPIO_PIN_PULLUP);
    nrf_gpio_cfg_output(11);
    nrf_gpio_cfg_output(12);

    /* Start 16 MHz crystal oscillator */
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART    = 1;

    /* Wait for the external oscillator to start up */
    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0)
    {
        // Do nothing.
    }

    // Configure TIMER_LED for generating modulation pattern
    nrf_drv_timer_config_t timer_cfg = {
        .frequency          = NRF_TIMER_FREQ_2MHz,\
        .mode               = NRF_TIMER_MODE_TIMER,          \
        .bit_width          = NRF_TIMER_BIT_WIDTH_32,\
        .interrupt_priority = APP_IRQ_PRIORITY_LOWEST,                    \
        .p_context          = NULL                                                       \
    };
    nrf_drv_timer_init(&TIMER_LED, &timer_cfg, timer_led_event_handler);
    nrf_drv_timer_extended_compare(
         &TIMER_LED, NRF_TIMER_CC_CHANNEL0, 1125, 0, true);
    nrf_drv_timer_extended_compare(
         &TIMER_LED, NRF_TIMER_CC_CHANNEL1, 4500, NRF_TIMER_SHORT_COMPARE1_CLEAR_MASK, true);
    // do not start the timer yet
//     nrf_drv_timer_enable(&TIMER_LED);


   // PWM on output 12 drives the IR LED
   // we use hardware PWM, the app_pwm_xxx uses a timer - so we don't use this
    NRF_PWM1->ENABLE = 1;

    NRF_PWM1->PRESCALER = 3;  // 2MHz
    NRF_PWM1->COUNTERTOP = 50;  // 25usec
    NRF_PWM1->MODE = 0;
    NRF_PWM1->DECODER = 0;
    NRF_PWM1->SHORTS = 0;
    
    NRF_PWM1->LOOP = 0;
    NRF_PWM1->PSEL.OUT[0] = 12; // output pin
    uint16_t seq[] = { 25, 1000, 1000, 1000}; // 25 = 12.5usec
    NRF_PWM1->SEQ[0].PTR = (uint32_t)seq;
    NRF_PWM1->SEQ[0].CNT = 1;

    NRF_PWM1->SEQ[0].REFRESH = 0;
    NRF_PWM1->SEQ[0].ENDDELAY = 0;
    // NRF_PWM1->TASKS_SEQSTART[0] = 1;  // start here
    
    // now create a timer to poll every 200msec the keys (keyrepetition)
    nrf_drv_timer_config_t timer_key_cfg = NRFX_TIMER_DEFAULT_CONFIG;
    nrf_drv_timer_init(&TIMER_KEY, &timer_key_cfg, timer_key_event_handler);
    uint32_t time_ticks = nrf_drv_timer_ms_to_ticks(&TIMER_KEY, 200);
    nrf_drv_timer_extended_compare(&TIMER_KEY, NRF_TIMER_CC_CHANNEL0, time_ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, true);
    nrf_drv_timer_enable(&TIMER_KEY);

/*    unsigned int oldB1 = nrf_gpio_pin_read(BUTTON_1);
    unsigned int oldB2 = nrf_gpio_pin_read(BUTTON_2);
    unsigned int oldB3 = nrf_gpio_pin_read(BUTTON_3);
    unsigned int oldB4 = nrf_gpio_pin_read(BUTTON_4);
*/
    while (true)
    {
       __WFI();
    }

}


/** @} */
